const mongoose = require('mongoose')
const validator = require('validator')
const postSchema = new mongoose.Schema({
    Sendername:{
        type: String,
        require:[true,'Please tell us your name'],
    },
    Sendercid:{
        type:Number,
        require:[true,'please provide a cid number!'],
        minlength:11,
        unique: true,
    },
    Senderaddress:{
        type: String,
        require:[true,'Please tell us your name'],
    },
    Destination:{
        type: String,
        require:[true,'Please tell us your name'],
    },
    Weightofparcel:{
        type:Number,
        require:[true,'please provide a cid number!'],
    },
    Phonenumber:{
        type:Number,
        require:[true,'please provide a cid number!'],
    },
    Offer:{
        type:Number,
        require:[true,'please provide a cid number!'],
    },
    Image:{
        type: String,
        default:'default.jpg',
    },
})

const Post = mongoose.model('Post',postSchema)
module.exports = Post