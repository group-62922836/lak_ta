const mongoose = require('mongoose')
const validator = require('validator')

const adminSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please tell us your name!'],
    },
    email:{
        type: String,
        require:[true,'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail,'Please provide a valid email'],
    },
    password:{
        type:String,
        require:[true,'please provide a password!'],
        minlength:8,
    }
})

const Admin = mongoose.model('Admin', adminSchema )
module.exports = Admin
