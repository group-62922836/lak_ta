const mongoose = require('mongoose')
const validator = require('validator')

const notifySchema = new mongoose.Schema({
    Sendername:{
        type: String,
        require:[true,'Please tell us your name'],
    },
    Sendercid:{
        type:Number,
        require:[true,'please provide a cid number!'],
        minlength:11,
    },
    Senderaddress:{
        type: String,
        require:[true,'Please tell us your address'],
    },
    Destination:{
        type: String,
        require:[true,'Please tell us your destination'],
    },
    Weightofparcel:{
        type:Number,
        require:[true,'please provide a weight!'],
    },
    Phonenumber:{
        type:Number,
        require:[true,'please provide a phone number!'],
    },
    Offer:{
        type:Number,
        require:[true,'please provide a offer!'],
    },
    Deliverername:{
        type: String,
        require:[true,'Please specify the Deliverername'],
    },
    Deliverercid:{
        type:Number,
        require:[true,'please provide a cid number!'],
        minlength:11,
    },
    Deliverernumber:{
        type:Number,
        require:[true,'please provide a phone number!'],
    },
    Carnumber:{
        type: String,
        require:[true,'Please specify the Deliverername'],
    },
})

const notify = mongoose.model('notify',notifySchema)
module.exports = notify