const express = require("express")
const path = require('path')
const app = express()

const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const serviceRouter = require('./routes/serviceRoutes')
const notifyRouter = require('./routes/notifyRoutes')
// const postRouter = require('./routes/postRoutes')
const adminRouter = require('./routes/adminRoutes') 

// app.use('/api/v1/service',serviceRouter)

// Middleware to parse JSON and handle URL-encoded form data
app.use(express.json());
app.use('/api/v1/service',serviceRouter)
// app.use(express.urlencoded({ extended: true }));

// Routes

app.use(express.json())
app.use('/api/v1/users',userRouter)

app.use(express.json())
app.use('/api/v1/notify',notifyRouter)

app.use(express.json())
app.use('/api/v1/admin',adminRouter)


// app.use('/api/v1/info',postRouter)
app.use('/',viewRouter)
// app.use('/',img)



app.use(express.static(path.join(__dirname,'views')))
// Serve uploaded image files
// app.use('/api/v1/uploads', express.static(path.join(__dirname, 'uploads')));
module.exports = app
