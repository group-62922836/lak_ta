const Notify =  require('./../models/notifyModels')

exports.getAllNotifys = async (req,res,next) => {
    try {
        const notifys = await Notify.find()
        res.status(200).json({data:notifys, status: 'success'})
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}
exports.createNotify = async (req,res) => {
    try {
        const notify = await Notify.create(req.body);
        console.log(req.body.name)
        res.json({data: notify, status: "success"});
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}
exports.getNotify = async (req,res) => {
    try {
        const notify = await Notify.findById(req.params.id)
        res.json({data: notify, status: "success"});
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}
exports.deleteNotify = async(req,res) => {
    try {
        const notify = await Notify.findByIdAndDelete(req.params.id);
        res.json({data:notify, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}
