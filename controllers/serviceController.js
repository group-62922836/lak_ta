const User = require('./../models/serviceModels')

const AppError = require('../utils/appError')

const multer = require('multer');


const multerStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'views/image/service');
    },
    filename: (req, file, cb) => {
        const ext = file.mimetype.split('/')[1];
        cb(null, `user-${Date.now()}.${ext}`)
    },
})
const multerFilter = (req, file, cb) => {
    if (file.mimetype.startsWith('image')) {
        cb(null, true);
    } else {
        cb(new AppError('Not an image! Please upload only images.', 400), false);
    }
};
const upload = multer({
    storage: multerStorage,
    fileFilter: multerFilter
});

exports.UploadserviceImage = upload.single('Image');


exports.getAllServices = async (req, res, next) => {
    try {
        const users = await User.find()
        res.status(200).json({ data: users, status: 'success' })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}



// Modify the createPost function
exports.createService = async (req, res) => {
    try {
        if (req.file) {
            req.body.Image = req.file.filename;
        }
        const user = await User.create(req.body);
        console.log(req.body.name)
        res.json({ data: user, status: 'success' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}


exports.getService = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        res.json({ data: user, status: 'success' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.updateService = async (req, res) => {
    try {
        // Check if there's a file in the request
        if (req.file) {
            req.body.Image = req.file.filename;
        }

        // Find the user by ID and update the fields
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {
            new: true, // Return the modified document rather than the original
            runValidators: true, // Run validators to ensure the updated data is valid
        });

        // Check if the user is found
        if (!user) {
            return res.status(404).json({ error: 'Service not found' });
        }

        res.json({ data: user, status: 'success update' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

// exports.updateService = async (req, res) => {
//     try {
//         const user = await User.findByIdAndUpdate(req.params.id, req.body);
//         res.json({ data: user, status: 'success update' });
//     } catch (err) {
//         res.status(500).json({ error: err.message });
//     }
// };

exports.deleteService = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        res.json({ data: user, status: 'success' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

