const User = require('./../models/postModels')
const AppError = require('../utils/appError')

// const multer = require('multer');



exports.getAllPost = async (req,res,next) =>{
    try{
        const users = await User.find()
        res.status(200).json({data:users, status:'success'})
    }   catch (err) {
        res.status(500).json({error: err.message});
    }
}
// ...

// Set up multer storage configuration
// const storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, 'uploads/'); // Specify the destination folder to store uploaded images
//   },
//   filename: (req, file, cb) => {
//     const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
//     cb(null, uniqueSuffix + '-' + file.originalname); // Generate a unique filename for the uploaded image
//   }
// });
// const upload = multer({ storage: storage });

// ...

// Modify the createPost function
exports.createPost = async (req,res) =>{
  try{
      const user = await User.create(req.body);
      console.log(req.body.name)
      res.json({data:user, status:'success'});
  }   catch (err) {
      res.status(500).json({error: err.message});
  }
}


exports.getPost = async (req,res) =>{
    try{
        const user = await User.findById(req.params.id);
        res.json({data:user, status:'success'});
    }   catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.updatePost = async (req, res) => {
    try {
        // Check if there's a file in the request
        if (req.file) {
            req.body.Image = req.file.filename;
        }

        // Find the user by ID and update the fields
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {
            new: true, // Return the modified document rather than the original
            runValidators: true, // Run validators to ensure the updated data is valid
        });

        // Check if the user is found
        if (!user) {
            return res.status(404).json({ error: 'Service not found' });
        }

        res.json({ data: user, status: 'success update' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.deletePost = async (req,res) =>{
    try{
        const user = await User.findByIdAndDelete(req.params.id);
        res.json({data:user, status:'success'});
    }   catch (err) {
        res.status(500).json({error: err.message});
    }
}
