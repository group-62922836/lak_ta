const path = require('path')

exports.getLanding = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','landingpage.html'))
}
exports.getservice = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','service.html'))
}
exports.getPost = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','post.html'))
}
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','signin.html'))
}

exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../','views','signup.html'))
}
exports.getAdminLogin = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','adminlogin.html'))
}
exports.getAdmindash1 = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','admindashboard1.html'))
}
exports.getAdmindash2 = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','admindashboard2.html'))
}
exports.getAdmindash3 = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','admindashboard3.html'))
}
exports.getNotify = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','notify.html'))
}
exports.getHome = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','home.html'))
}
