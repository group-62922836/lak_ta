const allUsers = async () => {
	try {
	  const res = await axios({
		method: 'GET',
		url: 'http://localhost:4001/api/v1/users',
	  });
	  displayService(res.data);
	  console.log("HHEGYYV")
	  console.log(res.data)
	} catch (err) {
	  console.log(err);
	}
  };
  allUsers();

  const displayService = (news) => {
	var arr = news.data;
	// console.log("Hihdjb")
	console.log(arr.length);
	const tableBody = document.querySelector('table')
	for (let i = 0; i < arr.length; i++) {
	  const element = arr[i];
	  const row = document.createElement('tr')
	  row.classList.add('data');
	  row.id = element._id

	  const cell1 = document.createElement('td')
	  cell1.textContent = `${element.name}`
	  row.appendChild(cell1)

	  const cell2 = document.createElement('td')
	  cell2.textContent = `${element.email}`
	  row.appendChild(cell2)

	  const cell4 = document.createElement('td')
	  cell4.textContent = `${element.cid}`
	  row.appendChild(cell4)

	  const deleteCell = document.createElement('td');
	  const deleteButton = document.createElement('button');
	  deleteButton.innerText = 'Delete';
	  deleteButton.classList.add('delete-button');
	  deleteCell.appendChild(deleteButton);
	  row.appendChild(deleteCell);
	  
	  tableBody.appendChild(row);
	//   console.log(element)
	}
  };
  document.addEventListener('click', async (event) => {
	if (event.target.classList.contains('delete-button')) {
	  var card = event.target.parentNode.parentNode;
	  var rowIndex = Array.from(card.parentNode.children).indexOf(card);
	  console.log(rowIndex);
	
	  var confirmation = confirm("Are you sure you want to delete this Data");
  
	if (confirmation) {
		try {
		  const res = await axios.delete(`http://localhost:4001/api/v1/users/${card.id}`);
		  console.log(res);
		  card.remove();
		} catch (err) {
		  console.log(err);
		}
	  }
	}
  });