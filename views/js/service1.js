const postForm = document.getElementById('postForm');
postForm.addEventListener('submit', async (event) => {
  event.preventDefault();

  const senderAddress = document.getElementById('senderAddress').value;
  const phoneNumber = document.getElementById('phoneNumber').value;
  const destination = document.getElementById('destination').value;
  const priceOffer = document.getElementById('priceOffer').value;
  const weight = document.getElementById('weightOfParcel').value;
  const img = document.getElementById('imageUpload').files[0];


  var cookieValue = document.cookie;
  var startIndex = cookieValue.indexOf('{')
  var endIndex = cookieValue.lastIndexOf('}')+1;

  var jsonStr = cookieValue.slice(startIndex,endIndex);
  var cookieObj = JSON.parse(jsonStr)

  console.log(cookieObj)
  var cid = cookieObj.cid
  var names = cookieObj.name

  console.log(names)

  console.log(cookieObj.cid)

  const formData = new FormData();
  formData.append('Sendername', names);
  formData.append('Sendercid', parseInt(cid));
  formData.append('Senderaddress', senderAddress);
  formData.append('Destination', destination);
  formData.append('Weightofparcel', parseInt(weight));
  formData.append('Phonenumber', parseInt(phoneNumber));
  formData.append('Offer', parseInt(priceOffer));
  formData.append('Image', img);
  const isAppended = formData.has('Sendername');
  console.log(isAppended); // true or false

  // console.log(formData)
  createPost(formData);
});

const createPost = async (data) => {
  try {
    const response = await fetch('http://localhost:4001/api/v1/service/upload', {
      method: 'POST',
      body: data
    });

    if (response.ok) {
      const responseData = await response.json();
      console.log(responseData); // Handle success response

      // Show popup message
      alert('Post created successfully');

      // Empty the field container
      document.getElementById('senderAddress').value = '';
      document.getElementById('phoneNumber').value = '';
      document.getElementById('destination').value = '';
      document.getElementById('priceOffer').value = '';
      document.getElementById('weightOfParcel').value = '';
      document.getElementById('imageUpload').value = '';
    } else {
      throw new Error('Failed to create post');
    }
  } catch (err) {
    console.log(err);
  }
};
