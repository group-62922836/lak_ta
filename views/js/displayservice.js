const allService = async () => {
  try {
    const res = await axios({
      method: 'GET',
      url: 'http://localhost:4001/api/v1/service',
    })
    displayService(res.data)
    // console.log(res)
  } catch (err) {
    console.log(err)
  }
}
allService()


const displayService = (news) => {
  var arr = news.data;
  console.log(arr);

  for (let i = 0; i < arr.length; i++) {
    const element = arr[i];
    var card = document.querySelector('.col').cloneNode(true)
    var el1 = card.querySelector('.card-img-top')
    el1.src = `image/service/${element.Image}`

    var el2 = card.querySelector('.card-title')
    el2.innerHTML = `Destination: ${element.Destination}<br> Address: ${element.Senderaddress}`

    var el3 = card.querySelector('.card-text') // Fix variable name to el3
    el3.innerHTML = `Sender Name: ${element.Sendername} <br>Sender CID: ${element.Sendercid} <br>Phonenumber: ${element.Phonenumber} <br>Weight: ${element.Weightofparcel} <br>Offer: ${element.Offer}`

    card.id = element._id; // Assign an ID to the card element

    console.log(typeof element.Sendercid)
    console.log(element.Sendercid)

    var cookieValue = document.cookie;
    var startIndex = cookieValue.indexOf('{')
    var endIndex = cookieValue.lastIndexOf('}')+1;

    var jsonStr = cookieValue.slice(startIndex,endIndex);
    var cookieObj = JSON.parse(jsonStr)

    var cidcard = cookieObj.cid

    if (element.Sendercid==cidcard){
      var deleteButton = document.createElement('button');
      deleteButton.innerText = 'Delete';
      deleteButton.classList.add('delete-button'); // Add a class to the delete button
      card.appendChild(deleteButton);

      var editButton = document.createElement('button');
      editButton.innerText = 'Edit';
      editButton.classList.add('edit-button'); // Add a class to the delete button
      editButton.setAttribute('data-toggle', 'modal');
      card.appendChild(editButton);
      editButton.setAttribute('data-target', '#myModal');
    }
    if (element.Sendercid!==cidcard){
      var addButton = document.createElement('button');
      addButton.innerText = 'Add';
      addButton.classList.add('add-button'); // Add a class to the delete button
      addButton.setAttribute('data-toggle', 'modal');
      card.appendChild(addButton);
      addButton.setAttribute('data-target', '#myModal2');
    }
    
    var card2 = document.querySelector('.col');
    card2.insertAdjacentElement('afterend', card);
  }
  document.querySelector('.col').remove();
};

document.addEventListener('click', async (event) => {
  if (event.target.classList.contains('delete-button')) {
    var card = event.target.parentNode;
    var rowIndex = Array.from(card.parentNode.children).indexOf(card);
    console.log(rowIndex);
    var cards = document.querySelectorAll('.col');
    var card = cards[rowIndex]
    // Rest of your code for deleting the row...
  var confirmation = confirm("Are you sure you want to delete this notification?");

    if (confirmation) {
      try {
        const res = await axios.delete(`http://localhost:4001/api/v1/service/${card.id}`);
        console.log(res);
        card.remove();
      } catch (err) {
        console.log(err);
      }
    }
  }
});
document.addEventListener('click', async (event) => {
  if (event.target.classList.contains('edit-button')){
    var card = event.target.parentNode;
    var rowIndex = Array.from(card.parentNode.children).indexOf(card);
    var cards = document.querySelectorAll('.col');
    var card = cards[rowIndex]
    try {
      const res = await axios.get(`http://localhost:4001/api/v1/service/${card.id}`);
      // console.log(res);
      var specificData = res.data; // Retrieve the specific data from the response
      var senderName = specificData.data.Sendername;
      var Sendercid = specificData.data.Sendercid;
      var Senderaddress = specificData.data.Senderaddress;
      var Destination = specificData.data.Destination;
      var Weight = specificData.data.Weightofparcel;
      var Phonenumber = specificData.data.Phonenumber;
      var Offer = specificData.data.Offer;


      console.log(senderName)

      var a1Input = document.querySelector('.a1'); // Select the AI input element
      a1Input.value = senderName;
      
      var a2Input = document.querySelector('.a2'); // Select the AI input element
      a2Input.value = Senderaddress;

      var a3Input = document.querySelector('.a3'); // Select the AI input element
      a3Input.value = Sendercid;

      var a4Input = document.querySelector('.a4'); // Select the AI input element
      a4Input.value = Phonenumber;

      var a5Input = document.querySelector('.a5'); // Select the AI input element
      a5Input.value = Destination;

      var a6Input = document.querySelector('.a6'); // Select the AI input element
      a6Input.value = Offer;

      var a7Input = document.querySelector('.a7'); // Select the AI input element
      a7Input.value = Weight;

    } catch (err) {
      console.log(err);
    }
    console.log(specificData)

    document.addEventListener('click', async (event) => {
      if (event.target.classList.contains('btn-primary')) {
        var Sendername = document.getElementById('senderName').value;
        var Senderaddress = document.getElementById('senderAddress').value;
        var senderCID = document.getElementById('senderCID').value;
        var phoneNumber = document.getElementById('phoneNumber').value;
        var Destination = document.getElementById('destination').value;
        var weight = document.getElementById('weightOfParcel').value;
        var Offer = document.getElementById('priceOffer').value;
        var img = specificData.data.Image;
    
        console.log(weight);
    
        try {
          const res = await axios.patch(`http://localhost:4001/api/v1/service/${card.id}`, {
            Sendername: Sendername,
            Sendercid: Senderaddress,
            Senderaddress: senderCID,
            Destination: Destination,
            Weightofparcel: weight,
            Phonenumber: phoneNumber,
            Offer: Offer,
            Image: img
          });
          console.log(res.data.data.data);
          // Handle the successful update here
        } catch (err) {
          console.log(err);
          // Handle the error here
        }
      }
    });
    
  }
})