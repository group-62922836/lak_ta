// import "../../node_modules/axios";
import { showAlert } from "./alert.js"

export const signup = async (name, email,PhoneNumber,cid,password,passwordConfirm) =>{
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4001/api/v1/users/signup',
            data:{
                name,
                email,
                PhoneNumber,
                cid,
                password,
                passwordConfirm,
            },
        })
       
        if (res.data.status === 'success'){
            showAlert('success','Account created successfully!')
            window.setTimeout(() => {
                location.assign('/')
            }, 1500)
        }
    } catch (err) {
        console.log(err)
        let message =
        typeof err.response !== 'undefined'
            ? err.response.data.message
            : err.message
        showAlert('error','Error: Passwords are not the same!', message)
    }
    
}

document.querySelector('.submitButton').addEventListener('click', (e) => {
    e.preventDefault()
    // alert(" i am aler")
    const name = document.getElementById('name').value 
    const email = document.getElementById('email').value
    const PhoneNumber = document.getElementById('phone').value
    const cid = document.getElementById('cid').value
    const password = document.getElementById('pass').value
    const passwordConfirm = document.getElementById('conpass').value


    signup(name, email,PhoneNumber,cid,password,passwordConfirm)
})