const express = require('express')
const userController = require('./../controllers/adminController')
const authController = require('./../controllers/authController')
const router = express.Router()


// router.post('/adminlogin',authController.adminsignin)
router 
    .route('/')
    .get(userController.getAllAdmin)
    .post(userController.createAdmin)

router 
    .route('/:id')
    .get(userController.getAdmin)

module.exports = router