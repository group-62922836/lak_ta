const express = require('express')
const notifyController = require('./../controllers/notifyController')
const router = express.Router()

router.post('/upload', notifyController.createNotify)
router
    .route('/')
    .get(notifyController.getAllNotifys)
    .post(notifyController.createNotify)

router
    .route('/:id')
    .get(notifyController.getNotify)
    .delete(notifyController.deleteNotify)

module.exports = router