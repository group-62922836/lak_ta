const express = require('express')
const router = express.Router()
const viewsController = require('./../controllers/viewController')

router.get('/',viewsController.getLanding)
router.get('/login',viewsController.getLoginForm)
router.get('/signup',viewsController.getSignupForm)
router.get('/service',viewsController.getservice)
router.get('/admin', viewsController.getAdminLogin)
router.get('/post',viewsController.getPost)

router.get('/admindash1',viewsController.getAdmindash1)
router.get('/admindash2',viewsController.getAdmindash2)
router.get('/admindash3',viewsController.getAdmindash3)
router.get('/notify', viewsController.getNotify)
router.get('/home', viewsController.getHome)

module.exports = router

